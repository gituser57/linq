﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Linq.Objects
{
    public class StorePrice
    {
        public int GoodId { get; set; }
        public string Shop { get; set; }
        public double Price { get; set; }
    }
}
