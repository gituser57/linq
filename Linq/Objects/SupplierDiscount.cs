﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Linq.Objects
{
    public class SupplierDiscount
    {
        public int SupplierId { get; set; }
        public string ShopName { get; set; }
        public double Discount { get; set; }
    }
}
